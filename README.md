## Problem Description

Recreating Sale Stock product catalog: https://www.salestockindonesia.com/products/category/dress. The scenario is focused on browsing the information.

Basic functionality requirements:
- User can browse products in infinite list
- User can view detailed information of each product
- User can view the image of each product

## Dev Environment Setup

```
React 16.8.6
```

### Installing NPM

#### Linux

To install NPM in Linux, please use the following commands:

1. Start by updating the packages list by typing:
```
sudo apt update
```
2. Install nodejs using the apt package manager:
```
sudo apt install nodejs
```
3. To be able to download npm packages, you also need to install npm, the Node.js package manager. To do so type:
```
sudo apt install npm
```
If you want to be more clearly, please follow this [link](https://linuxize.com/post/how-to-install-node-js-on-ubuntu-18.04/)

#### Mac

To install NPM in Mac, please use the following commands:

Install nodejs using Homebrew:
```
brew install node
```

If you want to be more clearly, please follow this [link](https://treehouse.github.io/installation-guides/mac/node-mac.html)

### Installing React

#### Linux & Mac

To install React globally, please use the following commands:
```
npm install -g create-react-app
```

To create a new app, please use the following commands:
```
npm init react-app my-app
```

## Test Instructions

Run following commands in terminal to run the test
```
npm test
```

### Check The Test Coverage. 

To Check The Test Coverage, Please Run The Following Command

```
npm test -- --coverage
```

## Run Instructions

To run this project, please run the following command
```
npm start
```

## How To Use

To check the application, please run the following command in your web browser
```
http://localhost:3000
```
