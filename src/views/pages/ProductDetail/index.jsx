import React, { memo, useState, useEffect } from 'react'
import { Container, Row } from 'reactstrap'

import ProductItem from 'views/modules/Products/Components/ProductItem'
import ProductItemEmpty from 'views/modules/Products/Components/ProductItemEmpty'
import { ProductWrapper } from 'views/modules/Products/style'

const ProductDetailPage = memo(({ location }) => {
  const { data, pathname } = location

  const [productDetail, setProductDetail] = useState(null)
  
  useEffect(() => {
		if (data === undefined) {
			fetch('http://localhost:3000/product-list.json')
			.then(response => response.json())
			.then((response) => {
        let path = pathname.replace('/products/', '')
        let productBySlug = response.data.filter(item => item.slug === path)
        if (productBySlug && productBySlug.length > 0) {
          setProductDetail(productBySlug[0])
        } else {
          setProductDetail({})
        }
			})
			.catch((error) => {
				if (error.response) {
					alert('Oopps, There is something wrong when access data.')
				}
			})
		} else {
      setProductDetail(data)
    }
  }, [data, pathname, setProductDetail])

  return (
    <ProductWrapper>
      <Container>
        { (productDetail && productDetail.slug)
          ? <Row><ProductItem {...productDetail} isSinglePage /></Row>
          : <ProductItemEmpty />
        }
      </Container>
    </ProductWrapper>
  )
})

export default ProductDetailPage
