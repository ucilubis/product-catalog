import React, { memo } from 'react'

import Products from 'views/modules/Products'

const ProductPage = memo(() => <Products />)

export default ProductPage
