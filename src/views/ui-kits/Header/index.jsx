import React, { memo } from 'react'
import { Link } from 'react-router-dom'

import logo from 'assets/images/logo.png'
import { HeaderWrapper } from './style'

const Header = memo(() => {
	const iconNav = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-hamburger-greyDark-21f420231b8d7c6375f01d246955a244.png'
	const iconSearch = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-search-greyDark-75485234b11747c9cab05016d0c0c4be.png'
	const iconHeart = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-heart-greyDark-74fa2bfb60cf3c250b268fc886ef3144.png'
	const iconBasket = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-cart-greyDark-8465cf6f65a64e2a8dbf6100d9f705d3.png'
	
	return (
		<HeaderWrapper>
			<div>
				<div className="header-center">
					<div><img src={iconNav} alt="" /></div>
					<div>
						<Link to={'/'}>
							<img src={logo} alt="Salestock logo" />
						</Link>
					</div>
					<div>
						<div><img src={iconSearch} alt="" /></div>
						<div><img src={iconHeart} alt="" /></div>
						<div><img src={iconBasket} alt="" /></div>
					</div>
				</div>
			</div>
		</HeaderWrapper>
	)
})

export default Header
