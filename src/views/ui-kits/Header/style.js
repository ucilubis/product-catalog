import styled from 'styled-components'

const HeaderWrapper = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  background: #fff;
  z-index: 777;
  background-image: linear-gradient(0deg, #e8e8e8, #e8e8e8 49%, transparent 51%);
  background-position: bottom;
  background-size: 100% .1rem;
  background-repeat: no-repeat;

  > div:first-child {
    max-width: 480px;
    margin: 0 auto;
    position: relative;

    > .header-center {
      position: relative;
      background-color: white;
      width: 100%;
      height: 56px;
      display: table;
      box-sizing: border-box;

      > div:first-child {
        padding: 8px;
        display: table-cell;
        cursor: pointer;
        vertical-align: middle;

        > img {
          width: 24px;
          height: 24px;
        }
      }

      > div:nth-child(2) {
        width: 100%;
        height: 32px;
        display: table-cell;
        box-sizing: border-box;
        vertical-align: middle;
        padding: 0 1.2rem;
        text-align: left;

        > a > img {
          width: 119px;
        }
      }

      > div:nth-child(3) {
        padding: 8px;
        display: inline-flex;
        vertical-align: middle;
        
        img {
          cursor: pointer;
          height: 40px;
          padding: 7px;
        }
      }
    }
  }
`

export { HeaderWrapper }
 