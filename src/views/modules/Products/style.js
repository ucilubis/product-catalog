import styled from 'styled-components'

const ProductWrapper = styled.div`
  .square {
    position: relative;
    width: 100%;
  }

  .square:after {
    content: '';
    display: block;
    padding-bottom: 100%;
  }

  .ProductItem__wrapper {
    background-color: #ffffff;
    text-align: left;
  }

  .ProductItem__image {
    border-radius: 0.2rem 0.2rem 0 0;
    width: 100%;
  }

  .ProductItem__title {
    font-size: 1rem;
    font-weight: 400;
  }

  .ProductItem__title a {
    color: rgb(82, 82, 82);
    display: block;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    text-decoration: none;
  }

  .ProductItem__size {
    margin: 4px 0;
    > div:first-child {
      background-color: rgb(232, 232, 232);
      padding: 3px 8px;
      display: table;
      > span {
        margin: 0px;
        vertical-align: middle;
        display: block;
        color: rgb(128, 128, 128);
        font-size: 12px;
        line-height: 18px;
        max-width: 322px;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
      }
    }
  }

  .ProductItem__price {
    font-size: 1.1rem;
    font-weight: 700;
    color: rgb(82, 82, 82);
    margin: 0;
  }

  .ProductItem__size-table {
    text-align: center;
  }

  .ProductItem__footer {
    padding: 0.5rem;
    display: flex;
    align-items: flex-start;
    color: rgb(128, 128, 128);
  }

  .ProductItem__footer > div {
    font-size: 0.75rem;
  }

  .ProductItem__footer > div:last-child {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    width: 100%;
  }

  .ProductItem__footer > div > span {
    font-weight: 700;
  }

  .ProductItem__footer-shareArea {
    padding: 8px 16px;
    > img {
      width: 24px;
      height: 24px;
      background-size: 100%;
      background-position: 0px 0px;
    }
  }

  .ProductItem__footer-buttonArea {
    .btn-red {
      height: 40px;
      padding: 7px 15px;
      border-color: rgb(172, 20, 90);
      border-width: 1px;
      border-style: solid;
      border-radius: 8px;
      background-color: rgb(172, 20, 90);
      font-size: 0.85rem;
      font-weight: 700;
    }
  }

  .ProductItem__detail {
    padding: 0.5rem;
    display: flex;
    flex-direction: column;
    color: rgb(82, 82, 82);
    > .ProductItem__title.single {
      font-size: 1.2rem;
      font-weight: 400;
    }
    > div {
      width: 100%;
      display: table;
      > .ProductItem__price.single {
        font-size: 1.3rem;
        color: rgb(172, 20, 90);
        font-size: 20px;
        line-height: 28px;
        font-weight: normal;
        display: table-cell;
        float: left;
      }
      > .ProductItem__tryagain {
        display: table-cell;
        vertical-align: middle;
        width: 140px;
        margin: 0px 16px;
        > img {
          width: 24px;
          height: 24px;
        }
        > span {
          color: rgb(48, 172, 48);
          font-size: 14px;
          line-height: 21px;
        }
      }
    }
  }

  .ProductItem__bahan {
    padding: 0.5rem;
    border-width: 0.5px 0;
    border-style: solid;
    border-color: #e8e8e8;
    > p {
      margin: 0;
    }
    > p:first-child {
      color: rgb(128, 128, 128);
      font-size: 12px;
      line-height: 18px;
    }
    > p:last-child {
      color: rgb(82, 82, 82);
      font-size: 14px;
      line-height: 21px;
    }
  }

  .ProductItem__color {
    padding: 0.5rem;
    > p {
      color: rgb(82, 82, 82);
      font-size: 14px;
      line-height: 21px;
      margin: 0;
    }
    > p:first-child {
      font-weight: 700;
    }
  }

  .ProductItem__sizes {
    padding: 0.5rem;
    > p, span {
      color: rgb(82, 82, 82);
      font-size: 14px;
      line-height: 21px;
      margin: 0;
    }
    > p {
      font-weight: 700;
      > span {
        font-weight: 400;
      }
    }
  }

  .ProductItem__panduan {
    padding: 0.5rem;
    color: rgb(82, 82, 82);
    font-size: 14px;
    line-height: 21px;
    margin: 0;
    border-bottom: 0.5px solid #e8e8e8;
    cursor: pointer;
    > img {
      width: 24px;
      height: 24px;
      margin-right: 0.5rem;
    }
  }

  .ProductItem__detailsize {
    padding: 0.8rem 0.5rem;
    color: rgb(82, 82, 82);
    font-size: 14px;
    line-height: 21px;
    border-bottom: 0.5px solid #e8e8e8;
    display: flex;
    justify-content: space-between;
    > img {
      width: 24px;
      height: 24px;
    }
  }

  .ProductItem__panduansize {
    padding: 0.8rem 0.5rem;
    color: rgb(82, 82, 82);
    font-size: 14px;
    line-height: 21px;
    border-bottom: 1rem solid #e8e8e8;
    > div:first-child {
      color: rgb(128, 128, 128);
      font-size: 12px;
      line-height: 18px;
    }
    > div:nth-child(2) {
      margin: 0.5rem 0;
    }
    .size_item {
      margin-bottom: 0.5rem;
      > p {
        margin: 0;
        text-transform: capitalize;
      }
    }
  }

  .ProductItem__image-wrapper {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: #eee;
  }

  .ProductItem__empty {
    display: flex;
    flex-direction: column;
    background-color: #ffffff;
    padding: 24px;
    max-width: 480px;
    margin: 0 auto;
    position: relative;
    > .ProductItem__title {
      display: block;
      color: rgb(82, 82, 82);
      font-size: 22px;
      line-height: 30px;
      text-align: left;
      margin-top: 1.2rem;
    }
    > div:nth-child(3) > p {
      font-size: 15px;
      margin-top: 1.2rem;
      line-height: 21px;
      color: rgb(82, 82, 82);
    }
    .ProductItem__empty-buttonArea > button {
      height: 40px;
      width: 100%;
      padding: 7px 15px;
      border-color: rgb(172, 20, 90);
      border-width: 1px;
      border-style: solid;
      border-radius: 8px;
      background-color: rgb(172, 20, 90);
      font-size: 0.8rem;
      font-weight: 600;
    }
  }

  .ProductItem__iconEmpty > img {
    width: 430px;
  }
`

export {
  ProductWrapper
}
