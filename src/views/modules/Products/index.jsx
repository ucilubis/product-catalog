import React, { memo, useState, useEffect } from 'react'
import { Container, Row } from 'reactstrap'

import ProductItem from './Components/ProductItem'
import { ProductWrapper } from './style'

const Product = memo(() => {
	const [products, setProducts] = useState([])

	useEffect(() => {
		if (products.length === 0) {
			fetch('http://localhost:3000/product-list.json')
			.then(response => response.json())
			.then((response) => {
				setProducts(response.data)
			})
			.catch((error) => {
				if (error.response) {
					alert('Oopps, There is something wrong when access data.')
				}
			})
		}
	}, [products])

	const productList = () => {
		const productList = products && products.length > 0
		? products.map(product => (
			<ProductItem {...product} key={product.slug} />
		))
		: (<div>Data is not found</div>)

		return productList
	}

	return (
		<ProductWrapper>
			<Container>
				<Row>{productList()}</Row>
			</Container>
		</ProductWrapper>
	)
})

export default Product
