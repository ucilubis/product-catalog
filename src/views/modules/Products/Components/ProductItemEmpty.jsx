import React, { memo } from 'react'
import { Button } from 'reactstrap'

const ProductItemEmpty = memo(() => {
  const iconEmpty = 'https://salestock-public-prod.freetls.fastly.net/assets/images/oopsss-transparent-lowercase-28f81cc4.png'

  return (
    <div className="ProductItem__empty">
      <div className="ProductItem__iconEmpty"><img src={iconEmpty} alt="" /></div>
      <div className="ProductItem__title">
        Halaman yang sista cari tidak bisa Sorabel temukan <span role="img" aria-label="Sad">😭</span>
      </div>
      <div><p>Coba dimulai lagi dari</p></div>
      <div className="ProductItem__empty-buttonArea">
        <Button className="btn-red" size="sm">
          HALAMAN BELANJA
        </Button>
      </div>
    </div>
  )
})

export default ProductItemEmpty
