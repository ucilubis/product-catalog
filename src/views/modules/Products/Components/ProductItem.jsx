import React, { memo, useState } from 'react'
import { Col, Button } from 'reactstrap'
import { Link } from 'react-router-dom'

import ProductItemSize from './ProductItemSize'
import ProductItemColor from './ProductItemColor'

const ProductItem = memo((props) => {
  const {
    name,
    slug,
    price,
    images,
    colors,
    sizes,
    isSinglePage
  } = props

  const iconGantungan = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-cdbb-green-1bd5de1523af79f96b6da5f5339d22b8.png'
  const iconHeart = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-heart-grey-0a895ac5bdf1f98aa48d5f85efc7679d.png'
  
  const [isPanduanDetail, setIsPanduanDetail] = useState(false)

  const imageSection = () => {
    const ImageSection = isSinglePage
    ? (
			<div className="square">
				<div className="ProductItem__image-wrapper">
					<img
						src={images[0]}
						className="ProductItem__image single"
						alt={name}
					/>
				</div>
			</div>
    )
    : (
			<div className="square">
				<div className="ProductItem__image-wrapper">
					<Link
            to={
              {
                pathname: `/products/${slug}`,
                data: { ...props }
              }
            }>
						{
							<img
								src={images[0]}
								className="ProductItem__image"
								alt={name}
							/>
						}
					</Link>
				</div>
			</div>
		)
		return ImageSection
  }

  const headerSection = () => {
    const formattedPrice = price.toLocaleString('id')
    let sizesAll = []
    sizes.map(item => sizesAll.push(item.size))
    const HeaderSection = isSinglePage
    ? (
			<div className="ProductItem__detail">
				<h3 className="ProductItem__title single">{name}</h3>
				<div>
          <p className="ProductItem__price single">{formattedPrice}</p>
          <div className="ProductItem__tryagain">
            <img src={iconGantungan} alt="" />
            <span>Bisa Coba Dulu</span>
          </div>
        </div>
			</div>
    )
    : (
      <div className="ProductItem__footer">
        <div>
          <div className="ProductItem__title">
            <Link
              to={
                {
                  pathname: `/products/${slug}`,
                  data: { ...props }
                }
              }>
              {name}
            </Link>
          </div>
          <div className="ProductItem__size">
            <div>
              <span>{sizesAll.join(', ')}</span>
            </div>
          </div>
          <p className="ProductItem__price">{formattedPrice}</p>
        </div>
        <div>
          <div className="ProductItem__footer-shareArea">
            <img src={iconHeart} alt="Like" />
          </div>
          <div className="ProductItem__footer-buttonArea">
            <Button className="btn-red" size="sm">
              BELI
            </Button>
          </div>
        </div>
      </div>
		)
		return HeaderSection
  }

  const handlePanduanDetail = () => {
    setIsPanduanDetail(!isPanduanDetail)
  }

  const descriptionSection = () => {
    const DescriptionSection = isSinglePage
    ? (
			<React.Fragment>
        <div className="ProductItem__bahan">
          <p>BAHAN UTAMA</p>
          <p>Katun Rayon lapis Furing</p>
        </div>
        <ProductItemColor
          sizes={sizes}
          colors={colors}
          isPanduanDetail={isPanduanDetail}
          handlePanduanDetail={handlePanduanDetail}
        />
				<ProductItemSize
          sizes={sizes}
          colors={colors}
          isPanduanDetail={isPanduanDetail}
          handlePanduanDetail={handlePanduanDetail}
        />
			</React.Fragment>
    )
    : null
		return DescriptionSection
  }

  return (
    <Col xs={12} md={{ size: 6, offset: 3 }}>
      <div className="ProductItem__wrapper">
        {imageSection()}
        {headerSection()}
        {descriptionSection()}
      </div>
    </Col>
  )
})

export default ProductItem
