import React, { memo } from 'react'

const ProductItemSize = memo(({
  sizes,
  colors,
  handlePanduanDetail
}) => {
  const iconPanduan = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-ruler-greyDark-c49950d4e64e4814a4259115cc9a4603.png'
  let sizesAll = []
  sizes.map(item => sizesAll.push(item.size))

  return (
    <React.Fragment>
      <div className="ProductItem__color">
        <p>Warna</p>
        {colors.map((item, key) => <p key={key.toString()}>{item}</p> )}
      </div>
      <div className="ProductItem__sizes">
        <p>Ukuran <span> {sizesAll.join(', ')}</span></p>
      </div>
      <div
        className="ProductItem__panduan"
        onClick={handlePanduanDetail}
      >
        <img src={iconPanduan} alt="" />
        <span>Panduan Ukuran</span>
      </div>
    </React.Fragment>
  )
})

export default ProductItemSize
