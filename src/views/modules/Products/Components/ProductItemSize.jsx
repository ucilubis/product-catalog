import React, { memo } from 'react'

const ProductItemSize = memo(({
  sizes,
  colors,
  isPanduanDetail,
  handlePanduanDetail
}) => {
  const iconUp = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-chevron-up-grey-d4f1be6f27707295cefd9bb85fa1818c.png'
  const iconDown = 'https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-chevron-down-grey-9fa9af8fe2ac7664508e128add866a87.png'

  let sizesAll = Object.keys(sizes[0])

  return (
    <React.Fragment>
      <div className="ProductItem__detailsize" onClick={handlePanduanDetail}>
        <div>Detail & Ukuran</div>
        <img src={isPanduanDetail ? iconUp : iconDown} alt="" />
      </div>
      { isPanduanDetail && (
        <div className="ProductItem__panduansize">
          <div>PANDUAN UKURAN</div>
          <div>Warna: {colors.join(' ')}</div>
          { sizes.map((item, key) => (
            <div key={key.toString()} className="size_item">
              {sizesAll.map((itemSize, keySize) => (
                <p key={keySize.toString()}>{itemSize.replace('-', ' ')} {item[itemSize]}</p>
              ))}
            </div>
          ))}
        </div>
      )}
    </React.Fragment>
  )
})

export default ProductItemSize
