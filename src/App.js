import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Header from 'views/ui-kits/Header'
import Products from 'views/pages/Products'
import ProductDetail from 'views/pages/ProductDetail'
import './App.css'

const App = () => (
	<Router>
		<div className="App">
			<Header />
			<div>
				<Route exact path="/" component={Products} />
				<Route path="/products/:slug" component={ProductDetail} />
			</div>
		</div>
	</Router>
)

export default App;
